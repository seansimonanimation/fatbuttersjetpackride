using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    Rigidbody ButtersRigidbody;

//get all objects for audio
    GameObject RocketR;
    GameObject RocketL; 
    AudioSource RocketAudioL;
    AudioSource RocketAudioR;
    [SerializeField] float thrust = 15.0f;
    [SerializeField] float rotThrust = 2.0f;
    [SerializeField] float startVolume = 0.0f;
    [SerializeField] float maxVolume = 0.025f;
    [SerializeField] float volumeSpeed = 0.2f;
    [SerializeField] ParticleSystem rightRocketParticles;
    [SerializeField] ParticleSystem leftRocketParticles;



    // Start is called before the first frame update
    void Start()
    {
        ButtersRigidbody = GetComponent<Rigidbody>();
        RocketL = GameObject.Find("leftCorgiRocketMesh");
        RocketAudioL = RocketL.GetComponent<AudioSource>();
        RocketR = GameObject.Find("rightCorgiRocketMesh 1");
        RocketAudioR = RocketR.GetComponent<AudioSource>();
        RocketAudioL.Stop();
        RocketAudioR.Stop();

    }

    // Update is called once per frame
    void Update()
    {
        ProcessThrust();
        ProcessRot();
    }


    void ProcessThrust(){

        if (Input.GetKey(KeyCode.Space)){
            ButtersRigidbody.AddRelativeForce(0,thrust * Time.deltaTime * 60,0);
            rocketEffectChange(true);
        } else {
            rocketEffectChange(false);
        }
    }
    void ProcessRot(){
        if(Input.GetKey(KeyCode.A) && Input.GetKey(KeyCode.D)){

        } else if (Input.GetKey(KeyCode.A)){
            applyRot(rotThrust);
        }
        else if(Input.GetKey(KeyCode.D)){
            applyRot(-rotThrust);
        }
    }
    void applyRot(float rotatThisFrame)
    {
        //The tutorial has rotations freezing and unfreezing, but keeping them frozen just works better for this particular setup.
        //ButtersRigidbody.freezeRotation = true; //freezing rotations so we can manually rotate.
        transform.Rotate(Vector3.forward * rotatThisFrame * Time.deltaTime * 60);
        //ButtersRigidbody.freezeRotation = false; //unfreezing those same rotations so the physics system can take over.
    }


//Audio actually turns on and off when at 0, depending on
//whether the rocket is thrusting or not.
//Otherwise it increases and decreases up until it hits a certain
//threshold. We use both left and right rockets for effect. It's pretty sweet.
//Values set are based on the left rocket. It's the driver.
    public void rocketEffectChange(bool toPlay){
        if (toPlay == true){
            rightRocketParticles.Play();
            leftRocketParticles.Play();
            if (!RocketAudioL.isPlaying){ 
                RocketAudioL.Play(); 
                RocketAudioR.Play(); 
                if (RocketAudioL.volume == 0){ 
                    RocketAudioL.volume = startVolume;
                    RocketAudioR.volume = startVolume * 2;
                }
            } else if (RocketAudioL.isPlaying && RocketAudioL.volume < maxVolume){
                RocketAudioL.volume += volumeSpeed * Time.deltaTime;
                RocketAudioR.volume += volumeSpeed * 2 * Time.deltaTime;
            }
        } else {
            rightRocketParticles.Stop();
            leftRocketParticles.Stop();
            if (RocketAudioL.isPlaying && RocketAudioL.volume > startVolume){
                RocketAudioL.volume -= volumeSpeed * Time.deltaTime;
                RocketAudioR.volume -= volumeSpeed * 2 * Time.deltaTime;
            } else if (RocketAudioL.isPlaying && RocketAudioL.volume == startVolume){
                RocketAudioL.Stop();
                RocketAudioR.Stop();
            }
        }
    }
}