using System;
using UnityEngine;
using UnityEngine.SceneManagement;


public class CollisionHandler : MonoBehaviour
{
    [SerializeField] float invokeDelay = 2.0f;
    [SerializeField] AudioClip explosionSound;
    [SerializeField] AudioClip successSound;

    [SerializeField] ParticleSystem explosionParticle;
    [SerializeField] ParticleSystem successParticle;
    AudioSource corgiAudio;
    bool isTransitioning = false;

    void Start(){
        corgiAudio = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision other) {

        if(isTransitioning){return;} 
        else {
            switch (other.gameObject.tag){
                case "Friendly":
                    break;
                case "Finish":
                        startSuccessSequence();
                    break;
                default:
                StartCrashSequence();
                    break;
            }
        }
    }

    private void startSuccessSequence()
    {
        GameObject.Find("leftCorgiRocketMesh").GetComponent<AudioSource>().enabled = false;
        GameObject.Find("rightCorgiRocketMesh 1").GetComponent<AudioSource>().enabled = false;
        corgiAudio.PlayOneShot(successSound, 0.1f);
        successParticle.Play();
        GetComponent<Movement>().enabled = false;
        isTransitioning = true;
        Invoke("LoadNextLevel",invokeDelay);

    }

    void StartCrashSequence(){
        //TODO: Add effects upon crash.
        //GetComponent<Movement>().rocketAudioChange(false);
        corgiAudio.PlayOneShot(explosionSound,0.1f);
        GameObject.Find("leftCorgiRocketMesh").GetComponent<AudioSource>().enabled = false;
        GameObject.Find("rightCorgiRocketMesh 1").GetComponent<AudioSource>().enabled = false;
        explosionParticle.Play();
        hideCorgi();
        GetComponent<Movement>().enabled = false;
        isTransitioning = true;
        Invoke("ReloadLevel",invokeDelay);
    }
    void ReloadLevel(){
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }
    void LoadNextLevel(){
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int NextSceneIndex = currentSceneIndex + 1;
        if (NextSceneIndex == SceneManager.sceneCountInBuildSettings){
            NextSceneIndex = 0;
        }
        SceneManager.LoadScene(NextSceneIndex);
    }
    void fuelHandler(GameObject fuelObject){
        fuelObject.GetComponent<MeshRenderer>().enabled = false;
        fuelObject.GetComponent<SphereCollider>().enabled = false;
    }
    void hideCorgi(){
        GameObject.Find("leftCorgiRocketBallsMesh").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("leftCorgiRocketMesh").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("rightCorgiRocketBallsMesh 1").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("rightCorgiRocketMesh 1").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("corgiGogglesMesh").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("corgiharnessballsMesh").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("corgiHarnessFanMesh").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("corgiHarnessMesh").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("corgiMesh").GetComponent<MeshRenderer>().enabled = false;
        GameObject.Find("glassesMesh").GetComponent<MeshRenderer>().enabled = false;
        
    }
}
