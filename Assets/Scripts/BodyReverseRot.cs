using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyReverseRot : MonoBehaviour
{
    public GameObject OriginalButters;
    public GameObject ReverseButters;
    public Transform ButtersTrans;
    public Transform OGButtersTrans;

    // Start is called before the first frame update
    void Start()
    {
        OriginalButters = GameObject.Find("rocketCorgiPrefab");
        OGButtersTrans = OriginalButters.transform;
        ButtersTrans = transform;

    }
        // Update is called once per frame
        void Update()
        {
                ButtersTrans.rotation = Quaternion.Euler(0,0,OGButtersTrans.rotation.z * -1);
        }



    }